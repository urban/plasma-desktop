# Translation of plasma_applet_org.kde.desktopcontainment.po into Serbian.
# Chusslove Illich <caslav.ilic@gmx.net>, 2014, 2015, 2016, 2017.
msgid ""
msgstr ""
"Project-Id-Version: plasma_applet_org.kde.desktopcontainment\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-08-19 01:57+0000\n"
"PO-Revision-Date: 2017-12-15 12:38+0100\n"
"Last-Translator: Chusslove Illich <caslav.ilic@gmx.net>\n"
"Language-Team: Serbian <kde-i18n-sr@kde.org>\n"
"Language: sr@ijekavianlatin\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"
"X-Environment: kde\n"

#: package/contents/config/config.qml:16
#, kde-format
msgid "Location"
msgstr "Lokacija"

#: package/contents/config/config.qml:23
#: package/contents/ui/FolderViewLayer.qml:393
#, kde-format
msgid "Icons"
msgstr "Ikonice"

#: package/contents/config/config.qml:30
#, kde-format
msgid "Filter"
msgstr "Filter"

#: package/contents/ui/BackButtonItem.qml:104
#, kde-format
msgid "Back"
msgstr "Nazad"

#: package/contents/ui/ConfigFilter.qml:63
#, fuzzy, kde-format
#| msgid "File types:"
msgid "Files:"
msgstr "Tipovi fajlova:"

# >> @option:radio (that is, semantics thereof)
#: package/contents/ui/ConfigFilter.qml:64
#, fuzzy, kde-format
#| msgid "Show All Files"
msgid "Show all"
msgstr "Prikaži sve fajlove"

# >> @option:radio (that is, semantics thereof)
#: package/contents/ui/ConfigFilter.qml:64
#, fuzzy, kde-format
#| msgid "Show Files Matching"
msgid "Show matching"
msgstr "Prikaži poklopljene fajlove"

# >> @option:radio (that is, semantics thereof)
#: package/contents/ui/ConfigFilter.qml:64
#, fuzzy, kde-format
#| msgid "Hide Files Matching"
msgid "Hide matching"
msgstr "Sakrij poklopljene fajlove"

#: package/contents/ui/ConfigFilter.qml:69
#, kde-format
msgid "File name pattern:"
msgstr "Obrazac imena fajla:"

#: package/contents/ui/ConfigFilter.qml:76
#, kde-format
msgid "File types:"
msgstr "Tipovi fajlova:"

#: package/contents/ui/ConfigFilter.qml:82
#, kde-format
msgid "Show hidden files:"
msgstr ""

# >> @title:column
#: package/contents/ui/ConfigFilter.qml:178
#, kde-format
msgid "File type"
msgstr "tip fajla"

# >> @title:column
#: package/contents/ui/ConfigFilter.qml:187
#, kde-format
msgid "Description"
msgstr "opis"

#: package/contents/ui/ConfigFilter.qml:200
#, kde-format
msgid "Select All"
msgstr "Izaberi sve"

# На дугмету које треба да је уско.
#: package/contents/ui/ConfigFilter.qml:210
#, kde-format
msgid "Deselect All"
msgstr "Poništi izbor"

#: package/contents/ui/ConfigIcons.qml:61
#, kde-format
msgid "Panel button:"
msgstr "Panelsko dugme:"

#: package/contents/ui/ConfigIcons.qml:67
#, kde-format
msgid "Use a custom icon"
msgstr "Posebna ikonica"

#: package/contents/ui/ConfigIcons.qml:100
#, fuzzy, kde-format
#| msgctxt "@item:inmenu Open icon chooser dialog"
#| msgid "Choose..."
msgctxt "@item:inmenu Open icon chooser dialog"
msgid "Choose…"
msgstr "Izaberi..."

#: package/contents/ui/ConfigIcons.qml:106
#, kde-format
msgctxt "@item:inmenu Reset icon to default"
msgid "Clear Icon"
msgstr "Očisti ikonicu"

#: package/contents/ui/ConfigIcons.qml:126
#, kde-format
msgid "Arrangement:"
msgstr "Raspored:"

#: package/contents/ui/ConfigIcons.qml:130
#, kde-format
msgctxt "@item:inlistbox arrangement of icons"
msgid "Left to Right"
msgstr ""

#: package/contents/ui/ConfigIcons.qml:131
#, kde-format
msgctxt "@item:inlistbox arrangement of icons"
msgid "Right to Left"
msgstr ""

#: package/contents/ui/ConfigIcons.qml:132
#, kde-format
msgctxt "@item:inlistbox arrangement of icons"
msgid "Top to Bottom"
msgstr ""

#: package/contents/ui/ConfigIcons.qml:141
#, fuzzy, kde-format
#| msgid "Align"
msgctxt "@item:inlistbox alignment of icons"
msgid "Align left"
msgstr "Poravnaj"

#: package/contents/ui/ConfigIcons.qml:142
#, fuzzy, kde-format
#| msgid "Align"
msgctxt "@item:inlistbox alignment of icons"
msgid "Align right"
msgstr "Poravnaj"

#: package/contents/ui/ConfigIcons.qml:157
#, kde-format
msgid "Lock in place"
msgstr "Zaključaj na mestu"

#: package/contents/ui/ConfigIcons.qml:171
#, kde-format
msgid "Sorting:"
msgstr "Ređanje:"

#: package/contents/ui/ConfigIcons.qml:179
#, kde-format
msgctxt "@item:inlistbox sort icons manually"
msgid "Manual"
msgstr ""

# >> @item:inlistbox Sorting:
#: package/contents/ui/ConfigIcons.qml:180
#, fuzzy, kde-format
#| msgid "Name"
msgctxt "@item:inlistbox sort icons by name"
msgid "Name"
msgstr "ime"

# >> @item:inlistbox Sorting:
#: package/contents/ui/ConfigIcons.qml:181
#, fuzzy, kde-format
#| msgid "Size"
msgctxt "@item:inlistbox sort icons by size"
msgid "Size"
msgstr "veličina"

# >> @item:inlistbox Sorting:
#: package/contents/ui/ConfigIcons.qml:182
#, fuzzy, kde-format
#| msgid "Type"
msgctxt "@item:inlistbox sort icons by file type"
msgid "Type"
msgstr "tip"

# >> @item:inlistbox Sorting:
#: package/contents/ui/ConfigIcons.qml:183
#, fuzzy, kde-format
#| msgid "Date"
msgctxt "@item:inlistbox sort icons by date"
msgid "Date"
msgstr "datum"

# >> @option:check
#: package/contents/ui/ConfigIcons.qml:194
#, fuzzy, kde-format
#| msgid "Descending"
msgctxt "@option:check sort icons in descending order"
msgid "Descending"
msgstr "Opadajuće"

# >> @option:check
#: package/contents/ui/ConfigIcons.qml:202
#, fuzzy, kde-format
#| msgid "Folders first"
msgctxt "@option:check sort icons with folders first"
msgid "Folders first"
msgstr "Prvo fascikle"

#: package/contents/ui/ConfigIcons.qml:216
#, fuzzy, kde-format
#| msgctxt "whether to use icon or list view"
#| msgid "View mode"
msgctxt "whether to use icon or list view"
msgid "View mode:"
msgstr "Režim prikaza"

#: package/contents/ui/ConfigIcons.qml:218
#, fuzzy, kde-format
#| msgid "List"
msgctxt "@item:inlistbox show icons in a list"
msgid "List"
msgstr "Spisak"

#: package/contents/ui/ConfigIcons.qml:219
#, kde-format
msgctxt "@item:inlistbox show icons in a grid"
msgid "Grid"
msgstr ""

#: package/contents/ui/ConfigIcons.qml:230
#, fuzzy, kde-format
#| msgid "Icon Size"
msgid "Icon size:"
msgstr "Veličina ikonica"

# >> @item:inrange Size:
#: package/contents/ui/ConfigIcons.qml:245
#, fuzzy, kde-format
#| msgid "Small"
msgctxt "@label:slider smallest icon size"
msgid "Small"
msgstr "male"

# >> @item:inrange Size:
#: package/contents/ui/ConfigIcons.qml:254
#, fuzzy, kde-format
#| msgid "Large"
msgctxt "@label:slider largest icon size"
msgid "Large"
msgstr "velike"

#: package/contents/ui/ConfigIcons.qml:263
#, kde-format
msgid "Label width:"
msgstr ""

#: package/contents/ui/ConfigIcons.qml:266
#, kde-format
msgctxt "@item:inlistbox how long a text label should be"
msgid "Narrow"
msgstr ""

# >> @item:inmenu Icon Size
#: package/contents/ui/ConfigIcons.qml:267
#, fuzzy, kde-format
#| msgid "Medium"
msgctxt "@item:inlistbox how long a text label should be"
msgid "Medium"
msgstr "srednje"

#: package/contents/ui/ConfigIcons.qml:268
#, kde-format
msgctxt "@item:inlistbox how long a text label should be"
msgid "Wide"
msgstr ""

#: package/contents/ui/ConfigIcons.qml:276
#, fuzzy, kde-format
#| msgid "Text lines"
msgid "Text lines:"
msgstr "Redovi teksta"

#: package/contents/ui/ConfigIcons.qml:292
#, kde-format
msgid "When hovering over icons:"
msgstr ""

# >> @option:check
#: package/contents/ui/ConfigIcons.qml:294
#, fuzzy, kde-format
#| msgid "Tool tips"
msgid "Show tooltips"
msgstr "Oblačići"

# >> @option:check
#: package/contents/ui/ConfigIcons.qml:301
#, fuzzy, kde-format
#| msgid "Selection markers"
msgid "Show selection markers"
msgstr "Markeri izbora"

# >> @option:check
#: package/contents/ui/ConfigIcons.qml:308
#, fuzzy, kde-format
#| msgid "Folder preview popups"
msgid "Show folder preview popups"
msgstr "Iskakači pregleda fascikle"

#: package/contents/ui/ConfigIcons.qml:318
#, kde-format
msgid "Rename:"
msgstr ""

#: package/contents/ui/ConfigIcons.qml:322
#, kde-format
msgid "Rename inline by clicking selected item's text"
msgstr ""

#: package/contents/ui/ConfigIcons.qml:333
#, fuzzy, kde-format
#| msgid "Preview Plugins"
msgid "Previews:"
msgstr "Priključci za pregled"

# >> @option:check
#: package/contents/ui/ConfigIcons.qml:335
#, fuzzy, kde-format
#| msgid "Preview thumbnails"
msgid "Show preview thumbnails"
msgstr "Sličice za pregled"

#: package/contents/ui/ConfigIcons.qml:343
#, fuzzy, kde-format
#| msgid "More Preview Options..."
msgid "Configure Preview Plugins…"
msgstr "Još opcija pregleda..."

#: package/contents/ui/ConfigLocation.qml:80
#, kde-format
msgid "Show:"
msgstr ""

# >> @option:radio
#: package/contents/ui/ConfigLocation.qml:82
#, fuzzy, kde-format
#| msgid "Show the Desktop folder"
msgid "Desktop folder"
msgstr "Prikaži fasciklu površi"

# >> @option:radio
#: package/contents/ui/ConfigLocation.qml:89
#, fuzzy, kde-format
#| msgid "Show files linked to the current activity"
msgid "Files linked to the current activity"
msgstr "Prikaži fajlove povezana sa trenutnom aktivnošću"

#: package/contents/ui/ConfigLocation.qml:96
#, kde-format
msgid "Places panel item:"
msgstr ""

# >> @item:inlistbox Title
#: package/contents/ui/ConfigLocation.qml:129
#, fuzzy, kde-format
#| msgid "Custom title"
msgid "Custom location:"
msgstr "poseban naslov"

#: package/contents/ui/ConfigLocation.qml:137
#, fuzzy, kde-format
#| msgid "Type a path or a URL here"
msgid "Type path or URL…"
msgstr "Unesite putanju ili URL"

#: package/contents/ui/ConfigLocation.qml:180
#, kde-format
msgid "Title:"
msgstr "Naslov:"

# >> @item:inlistbox Title
#: package/contents/ui/ConfigLocation.qml:182
#, kde-format
msgid "None"
msgstr "nikakav"

# >> @item:inlistbox Title
#: package/contents/ui/ConfigLocation.qml:182
#, kde-format
msgid "Default"
msgstr "podrazumevan"

# >> @item:inlistbox Title
#: package/contents/ui/ConfigLocation.qml:182
#, kde-format
msgid "Full path"
msgstr "puna putanja"

# >> @item:inlistbox Title
#: package/contents/ui/ConfigLocation.qml:182
#, kde-format
msgid "Custom title"
msgstr "poseban naslov"

#: package/contents/ui/ConfigLocation.qml:197
#, fuzzy, kde-format
#| msgid "Enter custom title here"
msgid "Enter custom title…"
msgstr "Unesite ovde poseban naslov"

#: package/contents/ui/ConfigOverlay.qml:86
#, kde-format
msgid "Click and drag to rotate"
msgstr ""

#: package/contents/ui/ConfigOverlay.qml:174
#, kde-format
msgid "Hide Background"
msgstr ""

#: package/contents/ui/ConfigOverlay.qml:174
#, kde-format
msgid "Show Background"
msgstr ""

#: package/contents/ui/ConfigOverlay.qml:222
#, kde-format
msgid "Remove"
msgstr "Ukloni"

#: package/contents/ui/FolderItemPreviewPluginsDialog.qml:19
#, kde-format
msgid "Preview Plugins"
msgstr "Priključci za pregled"

#: package/contents/ui/FolderView.qml:1196
#, kde-format
msgctxt "@info"
msgid ""
"There are a lot of files and folders on the desktop. This can cause bugs and "
"performance issues. Please consider moving some of them elsewhere."
msgstr ""

#: package/contents/ui/main.qml:387
#, fuzzy, kde-format
#| msgid "Configure Desktop"
msgid "Configure Desktop and Wallpaper…"
msgstr "Podesi površ"

# >> @title:window
#: plugins/folder/directorypicker.cpp:32
#, kde-format
msgid "Select Folder"
msgstr "Izbor fascikle"

#: plugins/folder/foldermodel.cpp:469
#, kde-format
msgid "&Refresh Desktop"
msgstr "&Osveži površ"

#: plugins/folder/foldermodel.cpp:469 plugins/folder/foldermodel.cpp:1625
#, kde-format
msgid "&Refresh View"
msgstr "&Osveži prikaz"

#: plugins/folder/foldermodel.cpp:1634
#, fuzzy, kde-format
#| msgid "&Empty Trash Bin"
msgid "&Empty Trash"
msgstr "&Isprazni smeće"

#: plugins/folder/foldermodel.cpp:1637
#, kde-format
msgctxt "Restore from trash"
msgid "Restore"
msgstr "Vrati"

#: plugins/folder/foldermodel.cpp:1640
#, kde-format
msgid "&Open"
msgstr "&Otvori"

#: plugins/folder/foldermodel.cpp:1757
#, kde-format
msgid "&Paste"
msgstr "&Nalepi"

#: plugins/folder/foldermodel.cpp:1874
#, kde-format
msgid "&Properties"
msgstr "&Svojstva"

#: plugins/folder/viewpropertiesmenu.cpp:21
#, kde-format
msgid "Sort By"
msgstr "Poređaj po"

# >> @item:inlistbox Sorting:
#: plugins/folder/viewpropertiesmenu.cpp:24
#, fuzzy, kde-format
#| msgid "Unsorted"
msgctxt "@item:inmenu Sort icons manually"
msgid "Unsorted"
msgstr "nepoređano"

# >> @item:inlistbox Sorting:
#: plugins/folder/viewpropertiesmenu.cpp:28
#, fuzzy, kde-format
#| msgid "Name"
msgctxt "@item:inmenu Sort icons by name"
msgid "Name"
msgstr "ime"

# >> @item:inlistbox Sorting:
#: plugins/folder/viewpropertiesmenu.cpp:32
#, fuzzy, kde-format
#| msgid "Size"
msgctxt "@item:inmenu Sort icons by size"
msgid "Size"
msgstr "veličina"

# >> @item:inlistbox Sorting:
#: plugins/folder/viewpropertiesmenu.cpp:36
#, fuzzy, kde-format
#| msgid "Type"
msgctxt "@item:inmenu Sort icons by file type"
msgid "Type"
msgstr "tip"

# >> @item:inlistbox Sorting:
#: plugins/folder/viewpropertiesmenu.cpp:40
#, fuzzy, kde-format
#| msgid "Date"
msgctxt "@item:inmenu Sort icons by date"
msgid "Date"
msgstr "datum"

# >> @option:check
#: plugins/folder/viewpropertiesmenu.cpp:45
#, fuzzy, kde-format
#| msgid "Descending"
msgctxt "@item:inmenu Sort icons in descending order"
msgid "Descending"
msgstr "Opadajuće"

# >> @item:inmenu Sort by
#: plugins/folder/viewpropertiesmenu.cpp:47
#, fuzzy, kde-format
#| msgid "Folders First"
msgctxt "@item:inmenu Sort icons with folders first"
msgid "Folders First"
msgstr "prvo fascikle"

#: plugins/folder/viewpropertiesmenu.cpp:50
#, kde-format
msgid "Icon Size"
msgstr "Veličina ikonica"

# >> @item:inmenu Icon Size
#: plugins/folder/viewpropertiesmenu.cpp:53
#, fuzzy, kde-format
#| msgid "Tiny"
msgctxt "@item:inmenu size of the icons"
msgid "Tiny"
msgstr "sićušne"

# >> @item:inrange Size:
#: plugins/folder/viewpropertiesmenu.cpp:54
#, fuzzy, kde-format
#| msgid "Small"
msgctxt "@item:inmenu size of the icons"
msgid "Very Small"
msgstr "male"

# >> @item:inrange Size:
#: plugins/folder/viewpropertiesmenu.cpp:55
#, fuzzy, kde-format
#| msgid "Small"
msgctxt "@item:inmenu size of the icons"
msgid "Small"
msgstr "male"

# >> @item:inmenu Icon Size
# rewrite-msgid: /Medium//
#: plugins/folder/viewpropertiesmenu.cpp:56
#, fuzzy, kde-format
#| msgid "Small Medium"
msgctxt "@item:inmenu size of the icons"
msgid "Small-Medium"
msgstr "male"

# >> @item:inmenu Icon Size
#: plugins/folder/viewpropertiesmenu.cpp:57
#, fuzzy, kde-format
#| msgid "Medium"
msgctxt "@item:inmenu size of the icons"
msgid "Medium"
msgstr "srednje"

# >> @item:inrange Size:
#: plugins/folder/viewpropertiesmenu.cpp:58
#, fuzzy, kde-format
#| msgid "Large"
msgctxt "@item:inmenu size of the icons"
msgid "Large"
msgstr "velike"

# >> @item:inmenu Icon Size
#: plugins/folder/viewpropertiesmenu.cpp:59
#, fuzzy, kde-format
#| msgid "Huge"
msgctxt "@item:inmenu size of the icons"
msgid "Huge"
msgstr "ogromne"

#: plugins/folder/viewpropertiesmenu.cpp:67
#, fuzzy, kde-format
#| msgid "Arrange In"
msgctxt "@item:inmenu arrangement of icons"
msgid "Arrange"
msgstr "Rasporedi po"

#: plugins/folder/viewpropertiesmenu.cpp:71
#, kde-format
msgctxt "@item:inmenu arrangement of icons"
msgid "Left to Right"
msgstr ""

#: plugins/folder/viewpropertiesmenu.cpp:72
#, kde-format
msgctxt "@item:inmenu arrangement of icons"
msgid "Right to Left"
msgstr ""

#: plugins/folder/viewpropertiesmenu.cpp:76
#, kde-format
msgctxt "@item:inmenu arrangement of icons"
msgid "Top to Bottom"
msgstr ""

#: plugins/folder/viewpropertiesmenu.cpp:81
#, kde-format
msgid "Align"
msgstr "Poravnaj"

# >> @item:inlistbox Align:
#: plugins/folder/viewpropertiesmenu.cpp:84
#, fuzzy, kde-format
#| msgid "Left"
msgctxt "@item:inmenu alignment of icons"
msgid "Left"
msgstr "lijevo"

# >> @item:inlistbox Align:
#: plugins/folder/viewpropertiesmenu.cpp:88
#, fuzzy, kde-format
#| msgid "Right"
msgctxt "@item:inmenu alignment of icons"
msgid "Right"
msgstr "desno"

#: plugins/folder/viewpropertiesmenu.cpp:93
#, kde-format
msgid "Show Previews"
msgstr ""

#: plugins/folder/viewpropertiesmenu.cpp:97
#, fuzzy, kde-format
#| msgid "Locked"
msgctxt "@item:inmenu lock icon positions in place"
msgid "Locked"
msgstr "Zaključano"

#~ msgid "Rotate"
#~ msgstr "Okreni"

#, fuzzy
#~| msgid "Search file type..."
#~ msgid "Search..."
#~ msgstr "Potraži tip fajla..."

# >> @item:inlistbox Arrange in:
#~ msgid "Rows"
#~ msgstr "vrstama"

# >> @item:inlistbox Arrange in:
#~ msgid "Columns"
#~ msgstr "kolonama"

#~ msgid "Features:"
#~ msgstr "Mogućnosti:"

#~ msgid "OK"
#~ msgstr "U redu"

#~ msgid "Cancel"
#~ msgstr "Odustani"
