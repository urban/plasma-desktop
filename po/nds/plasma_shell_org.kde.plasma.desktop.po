# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# Sönke Dibbern <s_dibbern@web.de>, 2014.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-08-18 01:56+0000\n"
"PO-Revision-Date: 2014-08-11 12:37+0200\n"
"Last-Translator: Sönke Dibbern <s_dibbern@web.de>\n"
"Language-Team: Low Saxon <kde-i18n-nds@kde.org>\n"
"Language: nds\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 1.4\n"

#: contents/activitymanager/ActivityItem.qml:205
msgid "Currently being used"
msgstr ""

#: contents/activitymanager/ActivityItem.qml:245
#, fuzzy
#| msgid "Stopped activities:"
msgid ""
"Move to\n"
"this activity"
msgstr "Anhollen Aktiviteten:"

#: contents/activitymanager/ActivityItem.qml:275
msgid ""
"Show also\n"
"in this activity"
msgstr ""

#: contents/activitymanager/ActivityItem.qml:337
msgid "Configure"
msgstr ""

#: contents/activitymanager/ActivityItem.qml:356
#, fuzzy
#| msgid "Stopped activities:"
msgid "Stop activity"
msgstr "Anhollen Aktiviteten:"

#: contents/activitymanager/ActivityList.qml:142
msgid "Stopped activities:"
msgstr "Anhollen Aktiviteten:"

#: contents/activitymanager/ActivityManager.qml:120
#, fuzzy
#| msgid "Create activity..."
msgid "Create activity…"
msgstr "Aktiviteet opstellen..."

#: contents/activitymanager/Heading.qml:59
msgid "Activities"
msgstr "Aktiviteten"

#: contents/activitymanager/StoppedActivityItem.qml:137
#, fuzzy
#| msgid "Create activity..."
msgid "Configure activity"
msgstr "Aktiviteet opstellen..."

#: contents/activitymanager/StoppedActivityItem.qml:154
msgid "Delete"
msgstr "Wegdoon"

#: contents/applet/AppletError.qml:128
msgid "Sorry! There was an error loading %1."
msgstr ""

#: contents/applet/AppletError.qml:166
msgid "Copy to Clipboard"
msgstr ""

#: contents/applet/AppletError.qml:189
msgid "View Error Details…"
msgstr ""

#: contents/applet/CompactApplet.qml:74
msgid "Open %1"
msgstr ""

#: contents/configuration/AboutPlugin.qml:19
#: contents/configuration/AppletConfiguration.qml:245
msgid "About"
msgstr ""

#: contents/configuration/AboutPlugin.qml:47
msgid "Send an email to %1"
msgstr ""

#: contents/configuration/AboutPlugin.qml:61
msgctxt "@info:tooltip %1 url"
msgid "Open website %1"
msgstr ""

#: contents/configuration/AboutPlugin.qml:129
msgid "Copyright"
msgstr ""

#: contents/configuration/AboutPlugin.qml:147 contents/explorer/Tooltip.qml:92
msgid "License:"
msgstr "Verlööfnis:"

#: contents/configuration/AboutPlugin.qml:150
msgctxt "@info:whatsthis"
msgid "View license text"
msgstr ""

#: contents/configuration/AboutPlugin.qml:164
#, fuzzy
#| msgid "Author:"
msgid "Authors"
msgstr "Autor:"

#: contents/configuration/AboutPlugin.qml:175
msgid "Credits"
msgstr ""

#: contents/configuration/AboutPlugin.qml:187
msgid "Translators"
msgstr ""

#: contents/configuration/AboutPlugin.qml:204
msgid "Report a Bug…"
msgstr ""

#: contents/configuration/AppletConfiguration.qml:56
#, fuzzy
#| msgid "Keyboard shortcuts"
msgid "Keyboard Shortcuts"
msgstr "Tastkombinatschonen"

#: contents/configuration/AppletConfiguration.qml:293
msgid "Apply Settings"
msgstr "Instellen bruken"

#: contents/configuration/AppletConfiguration.qml:294
msgid ""
"The settings of the current module have changed. Do you want to apply the "
"changes or discard them?"
msgstr ""
"De Instellen vun't aktive Moduul hebbt sik ännert. Wullt Du de Ännern bruken "
"oder wegsmieten?"

#: contents/configuration/AppletConfiguration.qml:324
msgid "OK"
msgstr "OK"

#: contents/configuration/AppletConfiguration.qml:332
msgid "Apply"
msgstr "Bruken"

#: contents/configuration/AppletConfiguration.qml:338
msgid "Cancel"
msgstr "Afbreken"

#: contents/configuration/ConfigCategoryDelegate.qml:27
msgid "Open configuration page"
msgstr ""

#: contents/configuration/ConfigurationContainmentActions.qml:21
msgid "Left-Button"
msgstr "Knoop links"

#: contents/configuration/ConfigurationContainmentActions.qml:22
msgid "Right-Button"
msgstr "Knoop rechts"

#: contents/configuration/ConfigurationContainmentActions.qml:23
msgid "Middle-Button"
msgstr "Middelknoop"

#: contents/configuration/ConfigurationContainmentActions.qml:24
#, fuzzy
#| msgid "Left-Button"
msgid "Back-Button"
msgstr "Knoop links"

#: contents/configuration/ConfigurationContainmentActions.qml:25
msgid "Forward-Button"
msgstr ""

#: contents/configuration/ConfigurationContainmentActions.qml:27
msgid "Vertical-Scroll"
msgstr "Pielrecht Rull"

#: contents/configuration/ConfigurationContainmentActions.qml:28
msgid "Horizontal-Scroll"
msgstr "Kimmrecht Rull"

#: contents/configuration/ConfigurationContainmentActions.qml:30
msgid "Shift"
msgstr "Ümschalt"

#: contents/configuration/ConfigurationContainmentActions.qml:31
msgid "Ctrl"
msgstr "Strg"

#: contents/configuration/ConfigurationContainmentActions.qml:32
msgid "Alt"
msgstr "Alt"

#: contents/configuration/ConfigurationContainmentActions.qml:33
msgid "Meta"
msgstr "Meta"

#: contents/configuration/ConfigurationContainmentActions.qml:98
msgctxt "Concatenation sign for shortcuts, e.g. Ctrl+Shift"
msgid "+"
msgstr ""

#: contents/configuration/ConfigurationContainmentActions.qml:170
msgctxt "@title"
msgid "About"
msgstr ""

#: contents/configuration/ConfigurationContainmentActions.qml:185
#: contents/configuration/MouseEventInputButton.qml:13
msgid "Add Action"
msgstr "Akschoon tofögen"

#: contents/configuration/ConfigurationContainmentAppearance.qml:67
msgid "Layout changes have been restricted by the system administrator"
msgstr ""

#: contents/configuration/ConfigurationContainmentAppearance.qml:82
msgid "Layout:"
msgstr "Utsehn:"

#: contents/configuration/ConfigurationContainmentAppearance.qml:96
#, fuzzy
#| msgid "Wallpaper Type:"
msgid "Wallpaper type:"
msgstr "Achtergrundbild-Typ:"

#: contents/configuration/ConfigurationContainmentAppearance.qml:116
msgid "Get New Plugins…"
msgstr ""

#: contents/configuration/ConfigurationContainmentAppearance.qml:184
msgid "Layout changes must be applied before other changes can be made"
msgstr ""

#: contents/configuration/ConfigurationContainmentAppearance.qml:188
#, fuzzy
#| msgid "Apply"
msgid "Apply Now"
msgstr "Bruken"

#: contents/configuration/ConfigurationShortcuts.qml:16
#, fuzzy
#| msgid "Keyboard shortcuts"
msgid "Shortcuts"
msgstr "Tastkombinatschonen"

#: contents/configuration/ConfigurationShortcuts.qml:28
msgid "This shortcut will activate the applet as though it had been clicked."
msgstr ""

#: contents/configuration/ContainmentConfiguration.qml:29
msgid "Wallpaper"
msgstr "Achtergrundbild"

#: contents/configuration/ContainmentConfiguration.qml:34
msgid "Mouse Actions"
msgstr "Muusakschonen"

#: contents/configuration/MouseEventInputButton.qml:20
msgid "Input Here"
msgstr "Hier wat ingeven"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:42
#, fuzzy
#| msgid "Apply Settings"
msgid "Panel Settings"
msgstr "Instellen bruken"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:48
#, fuzzy
#| msgid "Maximize Panel"
msgctxt "@action:button Make the panel as big as it can be"
msgid "Maximize"
msgstr "Paneel maximeren"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:52
msgid "Make this panel as tall as possible"
msgstr ""

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:53
msgid "Make this panel as wide as possible"
msgstr ""

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:61
#, fuzzy
#| msgid "Delete"
msgctxt "@action:button Delete the panel"
msgid "Delete"
msgstr "Wegdoon"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:64
msgid "Remove this panel; this action is undo-able"
msgstr ""

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:88
#, fuzzy
#| msgid "Panel Alignment"
msgid "Alignment:"
msgstr "Paneelutrichten"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:94
msgid "Top"
msgstr ""

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:94
msgid "Left"
msgstr "Links"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:95
msgid ""
"Aligns a non-maximized panel to the top; no effect when panel is maximized"
msgstr ""

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:95
msgid ""
"Aligns a non-maximized panel to the left; no effect when panel is maximized"
msgstr ""

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:103
msgid "Center"
msgstr "Merrn"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:104
msgid ""
"Aligns a non-maximized panel to the center; no effect when panel is maximized"
msgstr ""

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:112
msgid "Bottom"
msgstr ""

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:112
msgid "Right"
msgstr "Rechts"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:113
msgid ""
"Aligns a non-maximized panel to the bottom; no effect when panel is maximized"
msgstr ""

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:113
msgid ""
"Aligns a non-maximized panel to the right; no effect when panel is maximized"
msgstr ""

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:141
#, fuzzy
#| msgid "Visibility"
msgid "Visibility:"
msgstr "Sichtborkeit"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:146
msgid "Always Visible"
msgstr "Jümmers sichtbor"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:154
#, fuzzy
#| msgid "Auto Hide"
msgid "Auto-Hide"
msgstr "Automaatsch versteken"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:155
msgid ""
"Panel is hidden, but reveals itself when the cursor touches the panel's "
"screen edge"
msgstr ""

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:182
msgid "Opacity:"
msgstr ""

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:187
msgid "Always Opaque"
msgstr ""

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:195
msgid "Adaptive"
msgstr ""

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:196
msgid ""
"Panel is opaque when any windows are touching it, and translucent at other "
"times"
msgstr ""

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:205
#, fuzzy
#| msgid "Always Visible"
msgid "Always Translucent"
msgstr "Jümmers sichtbor"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:232
msgid "Floating:"
msgstr ""

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:237
msgid "Floating"
msgstr ""

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:238
msgid "Panel visibly floats away from its screen edge"
msgstr ""

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:247
msgid "Attached"
msgstr ""

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:248
msgid "Panel is attached to its screen edge"
msgstr ""

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:270
#, fuzzy
#| msgid "Keyboard shortcuts"
msgid "Focus Shortcut:"
msgstr "Tastkombinatschonen"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:280
msgid "Press this keyboard shortcut to move focus to the Panel"
msgstr ""

#: contents/configuration/panelconfiguration/Ruler.qml:20
msgid "Drag to change maximum height."
msgstr ""

#: contents/configuration/panelconfiguration/Ruler.qml:20
msgid "Drag to change maximum width."
msgstr ""

#: contents/configuration/panelconfiguration/Ruler.qml:20
#: contents/configuration/panelconfiguration/Ruler.qml:21
msgid "Double click to reset."
msgstr ""

#: contents/configuration/panelconfiguration/Ruler.qml:21
msgid "Drag to change minimum height."
msgstr ""

#: contents/configuration/panelconfiguration/Ruler.qml:21
msgid "Drag to change minimum width."
msgstr ""

#: contents/configuration/panelconfiguration/Ruler.qml:65
msgid ""
"Drag to change position on this screen edge.\n"
"Double click to reset."
msgstr ""

#: contents/configuration/panelconfiguration/ToolBar.qml:25
#, fuzzy
#| msgid "Add Widgets..."
msgid "Add Widgets…"
msgstr "Lüttprogrammen tofögen..."

#: contents/configuration/panelconfiguration/ToolBar.qml:26
msgid "Add Spacer"
msgstr "Platzmaker tofögen"

#: contents/configuration/panelconfiguration/ToolBar.qml:27
#, fuzzy
#| msgid "More Settings..."
msgid "More Options…"
msgstr "Anner Instellen..."

#: contents/configuration/panelconfiguration/ToolBar.qml:225
msgctxt "Minimize the length of this string as much as possible"
msgid "Drag to move"
msgstr ""

#: contents/configuration/panelconfiguration/ToolBar.qml:264
msgctxt "@info:tooltip"
msgid "Use arrow keys to move the panel"
msgstr ""

#: contents/configuration/panelconfiguration/ToolBar.qml:285
msgid "Panel width:"
msgstr ""

#: contents/configuration/panelconfiguration/ToolBar.qml:285
#, fuzzy
#| msgid "Panel Alignment"
msgid "Panel height:"
msgstr "Paneelutrichten"

#: contents/configuration/panelconfiguration/ToolBar.qml:405
#: contents/configuration/ShellContainmentConfiguration.qml:43
msgid "Close"
msgstr "Tomaken"

#: contents/configuration/ShellContainmentConfiguration.qml:19
msgid "Panels and Desktops Management"
msgstr ""

#: contents/configuration/ShellContainmentConfiguration.qml:34
msgid ""
"You can drag Panels and Desktops around to move them to different screens."
msgstr ""

#: contents/configuration/shellcontainmentconfiguration/Delegate.qml:178
msgid "Swap with Desktop on Screen %1"
msgstr ""

#: contents/configuration/shellcontainmentconfiguration/Delegate.qml:179
msgid "Move to Screen %1"
msgstr ""

#: contents/configuration/shellcontainmentconfiguration/Delegate.qml:193
#, fuzzy
#| msgid "Remove Panel"
msgid "Remove Desktop"
msgstr "Paneel wegmaken"

#: contents/configuration/shellcontainmentconfiguration/Delegate.qml:194
msgid "Remove Panel"
msgstr "Paneel wegmaken"

#: contents/configuration/shellcontainmentconfiguration/Delegate.qml:267
msgid "%1 (primary)"
msgstr ""

#: contents/explorer/AppletAlternatives.qml:64
#, fuzzy
#| msgid "Alternatives"
msgid "Alternative Widgets"
msgstr "Alternativen"

#: contents/explorer/AppletDelegate.qml:167
#, fuzzy
#| msgid "Uninstall"
msgid "Undo uninstall"
msgstr "Wegmaken"

#: contents/explorer/AppletDelegate.qml:168
#, fuzzy
#| msgid "Uninstall"
msgid "Uninstall widget"
msgstr "Wegmaken"

#: contents/explorer/Tooltip.qml:101
msgid "Author:"
msgstr "Autor:"

#: contents/explorer/Tooltip.qml:109
msgid "Email:"
msgstr "Nettpostadress:"

#: contents/explorer/Tooltip.qml:128
msgid "Uninstall"
msgstr "Wegmaken"

#: contents/explorer/WidgetExplorer.qml:118
#: contents/explorer/WidgetExplorer.qml:192
#, fuzzy
#| msgid "Widgets"
msgid "All Widgets"
msgstr "Lüttprogrammen"

#: contents/explorer/WidgetExplorer.qml:144
msgid "Widgets"
msgstr "Lüttprogrammen"

#: contents/explorer/WidgetExplorer.qml:152
#, fuzzy
#| msgid "Get new widgets"
msgid "Get New Widgets…"
msgstr "Nieg Lüttprogrammen halen"

#: contents/explorer/WidgetExplorer.qml:203
msgid "Categories"
msgstr "Kategorien"

#: contents/explorer/WidgetExplorer.qml:283
msgid "No widgets matched the search terms"
msgstr ""

#: contents/explorer/WidgetExplorer.qml:283
msgid "No widgets available"
msgstr ""

#~ msgid "Switch"
#~ msgstr "Wesseln"

#, fuzzy
#~| msgid "Windows Go Below"
#~ msgid "Windows Above"
#~ msgstr "Finstern blievt dor achter"

#, fuzzy
#~| msgid "Windows Go Below"
#~ msgid "Windows Below"
#~ msgstr "Finstern blievt dor achter"

#, fuzzy
#~| msgid "Windows Can Cover"
#~ msgid "Windows In Front"
#~ msgstr "Finstern dörvt dor över"

#, fuzzy
#~| msgid "Search..."
#~ msgid "Search…"
#~ msgstr "Söken..."

#~ msgid "Screen Edge"
#~ msgstr "Schirmkant"

#~ msgid "Width"
#~ msgstr "Breed"

#~ msgid "Height"
#~ msgstr "Hööchde"

#~ msgid "Lock Widgets"
#~ msgstr "Lüttprogrammen afsluten"

#~ msgid "Activity name:"
#~ msgstr "Aktiviteet-Naam:"

#~ msgid "Create"
#~ msgstr "Opstellen"

#, fuzzy
#~| msgid "Are you sure you want to delete the activity?"
#~ msgid "Are you sure you want to delete this activity?"
#~ msgstr "Wullt Du de Aktiviteet redig wegdoon?"

#~ msgctxt "org.kde.plasma.desktop"
#~ msgid "Ok"
#~ msgstr "OK"

#~ msgctxt "org.kde.plasma.desktop"
#~ msgid "Apply"
#~ msgstr "Bruken"

#~ msgctxt "org.kde.plasma.desktop"
#~ msgid "Cancel"
#~ msgstr "Afbreken"
