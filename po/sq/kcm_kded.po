# Albanian translation for kdebase-runtime
# Copyright (c) 2010 Rosetta Contributors and Canonical Ltd 2010
# This file is distributed under the same license as the kdebase-runtime package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2010.
#
msgid ""
msgstr ""
"Project-Id-Version: kdebase-runtime\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-06-29 02:07+0000\n"
"PO-Revision-Date: 2010-01-30 05:41+0000\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: Albanian <sq@li.org>\n"
"Language: sq\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Launchpad-Export-Date: 2011-04-22 15:09+0000\n"
"X-Generator: Launchpad (build 12883)\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: kcmkded.cpp:115
#, kde-format
msgid "Failed to stop service: %1"
msgstr ""

#: kcmkded.cpp:117
#, kde-format
msgid "Failed to start service: %1"
msgstr ""

#: kcmkded.cpp:124
#, kde-format
msgid "Failed to stop service."
msgstr ""

#: kcmkded.cpp:126
#, kde-format
msgid "Failed to start service."
msgstr ""

#: kcmkded.cpp:224
#, kde-format
msgid "Failed to notify KDE Service Manager (kded6) of saved changed: %1"
msgstr ""

#: ui/main.qml:36
#, kde-format
msgid ""
"The background services manager (kded6) is currently not running. Make sure "
"it is installed correctly."
msgstr ""

#: ui/main.qml:45
#, kde-format
msgid ""
"Some services disable themselves again when manually started if they are not "
"useful in the current environment."
msgstr ""

#: ui/main.qml:54
#, kde-format
msgid ""
"Some services were automatically started/stopped when the background "
"services manager (kded6) was restarted to apply your changes."
msgstr ""

#: ui/main.qml:96
#, fuzzy, kde-format
#| msgid "Service"
msgid "All Services"
msgstr "Shërbimi"

#: ui/main.qml:97
#, fuzzy, kde-format
#| msgid "Running"
msgctxt "List running services"
msgid "Running"
msgstr "Në ekzekutim"

#: ui/main.qml:98
#, fuzzy, kde-format
#| msgid "Running"
msgctxt "List not running services"
msgid "Not Running"
msgstr "Në ekzekutim"

#: ui/main.qml:134
#, fuzzy, kde-format
#| msgid "Service"
msgid "Startup Services"
msgstr "Shërbimi"

#: ui/main.qml:135
#, kde-format
msgid "Load-on-Demand Services"
msgstr ""

#: ui/main.qml:152
#, kde-format
msgid "Toggle automatically loading this service on startup"
msgstr ""

#: ui/main.qml:219
#, fuzzy, kde-format
#| msgid "Running"
msgid "Not running"
msgstr "Në ekzekutim"

#: ui/main.qml:220
#, kde-format
msgid "Running"
msgstr "Në ekzekutim"

#: ui/main.qml:238
#, fuzzy, kde-format
#| msgid "Service"
msgid "Stop Service"
msgstr "Shërbimi"

#: ui/main.qml:238
#, fuzzy, kde-format
#| msgid "Service"
msgid "Start Service"
msgstr "Shërbimi"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "KDE Shqip, Launchpad Contributions: Vilson Gjeci"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "kde-shqip@yahoogroups.com,vilsongjeci@gmail.com"

#, fuzzy
#~| msgid "(c) 2002 Daniel Molkentin"
#~ msgid "(c) 2002 Daniel Molkentin, (c) 2020 Kai Uwe Broulik"
#~ msgstr "(c) 2002 Daniel Molkentin"

#~ msgid "Daniel Molkentin"
#~ msgstr "Daniel Molkentin"

#~ msgid "kcmkded"
#~ msgstr "kcmkded"

#~ msgid "KDE Service Manager"
#~ msgstr "Menaxhuesi i Shërbimeve KDE"

#~ msgid "Status"
#~ msgstr "Gjendja"

#~ msgid "Description"
#~ msgstr "Përshkrimi"

#~ msgid "Use"
#~ msgstr "Përdorimi"

#~ msgid "Start"
#~ msgstr "Nisja"

#~ msgid "Stop"
#~ msgstr "Ndalo"
