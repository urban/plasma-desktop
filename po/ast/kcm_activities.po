# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-desktop package.
#
# Enol P. <enolp@softastur.org>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: plasma-desktop\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-06-09 02:14+0000\n"
"PO-Revision-Date: 2023-05-03 21:41+0200\n"
"Last-Translator: Enol P. <enolp@softastur.org>\n"
"Language-Team: \n"
"Language: ast\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 23.04.0\n"

#: ui/ActivityEditor.qml:22
#, kde-format
msgctxt "@title:window"
msgid "Activity Settings for %1"
msgstr ""

#: ui/ActivityEditor.qml:23
#, kde-format
msgctxt "@title:window"
msgid "Create a New Activity"
msgstr ""

#: ui/ActivityEditor.qml:26
#, kde-format
msgctxt "@action:button as in, 'save changes'"
msgid "Save"
msgstr ""

#: ui/ActivityEditor.qml:57
#, kde-format
msgctxt "@label:chooser"
msgid "Icon:"
msgstr ""

#: ui/ActivityEditor.qml:68
#, kde-format
msgctxt "@label:textbox"
msgid "Name:"
msgstr ""

#: ui/ActivityEditor.qml:74
#, kde-format
msgctxt "@label:textbox"
msgid "Description:"
msgstr ""

#: ui/ActivityEditor.qml:84
#, kde-format
msgctxt "@option:check"
msgid "Privacy:"
msgstr ""

#: ui/ActivityEditor.qml:85
#, kde-format
msgid "Do not track usage for this activity"
msgstr ""

#: ui/ActivityEditor.qml:91
#, kde-format
msgid "Shortcut for switching:"
msgstr ""

#: ui/main.qml:55
#, kde-format
msgctxt "@info:tooltip"
msgid "Configure %1 activity"
msgstr ""

#: ui/main.qml:62
#, kde-format
msgctxt "@info:tooltip"
msgid "Delete %1 activity"
msgstr ""

#: ui/main.qml:75
#, kde-format
msgid "Create New…"
msgstr ""

#: ui/main.qml:86
#, kde-format
msgctxt "@title:window"
msgid "Delete Activity"
msgstr ""

#: ui/main.qml:88
#, kde-format
msgctxt "%1 is an activity name"
msgid "Do you want to delete activity '%1'?"
msgstr ""

#: ui/main.qml:93
#, kde-format
msgid "Delete Activity"
msgstr ""

#~ msgid "Activities"
#~ msgstr "Actividaes"
