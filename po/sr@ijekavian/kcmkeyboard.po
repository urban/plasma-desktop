# Translation of kcmkeyboard.po into Serbian.
# Chusslove Illich <caslav.ilic@gmx.net>, 2007, 2008, 2010, 2011, 2012, 2014, 2016, 2017.
# Dalibor Djuric <daliborddjuric@gmail.com>, 2009, 2010.
msgid ""
msgstr ""
"Project-Id-Version: kcmkeyboard\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-06-04 02:17+0000\n"
"PO-Revision-Date: 2017-09-28 17:58+0200\n"
"Last-Translator: Chusslove Illich <caslav.ilic@gmx.net>\n"
"Language-Team: Serbian <kde-i18n-sr@kde.org>\n"
"Language: sr@ijekavian\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"
"X-Environment: kde\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Часлав Илић"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "caslav.ilic@gmx.net"

#: bindings.cpp:24
#, fuzzy, kde-format
#| msgid "KDE Keyboard Layout Switcher"
msgid "Keyboard Layout Switcher"
msgstr "КДЕ‑ов мењач распореда тастатуре"

#: bindings.cpp:26
#, kde-format
msgid "Switch to Next Keyboard Layout"
msgstr "Пребаци на следећи распоред тастера"

#: bindings.cpp:49
#, kde-format
msgid "Switch keyboard layout to %1"
msgstr "Пребаци распоред тастатуре на %1"

#: flags.cpp:77
#, kde-format
msgctxt "layout - variant"
msgid "%1 - %2"
msgstr "%1 — %2"

#. i18n: ectx: property (windowTitle), widget (QDialog, AddLayoutDialog)
#: kcm_add_layout_dialog.ui:14
#, kde-format
msgid "Add Layout"
msgstr "Додавање распореда"

#. i18n: ectx: property (placeholderText), widget (QLineEdit, layoutSearchField)
#: kcm_add_layout_dialog.ui:20
#, kde-format
msgid "Search…"
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, shortcutLabel)
#: kcm_add_layout_dialog.ui:45
#, kde-format
msgid "Shortcut:"
msgstr "Пречица:"

#. i18n: ectx: property (text), widget (QLabel, labelLabel)
#: kcm_add_layout_dialog.ui:55
#, kde-format
msgid "Label:"
msgstr "Етикета:"

#. i18n: ectx: property (text), widget (QPushButton, prevbutton)
#. i18n: ectx: property (text), widget (QPushButton, previewButton)
#: kcm_add_layout_dialog.ui:76 kcm_keyboard.ui:315
#, kde-format
msgid "Preview"
msgstr "Преглед"

#. i18n: ectx: attribute (title), widget (QWidget, tabHardware)
#: kcm_keyboard.ui:18
#, kde-format
msgid "Hardware"
msgstr "Хардвер"

#. i18n: ectx: property (text), widget (QLabel, label_4)
#: kcm_keyboard.ui:33
#, kde-format
msgid "Keyboard &model:"
msgstr "&Модел тастатуре:"

#. i18n: ectx: property (whatsThis), widget (QComboBox, keyboardModelComboBox)
#: kcm_keyboard.ui:53
#, kde-format
msgid ""
"Here you can choose a keyboard model. This setting is independent of your "
"keyboard layout and refers to the \"hardware\" model, i.e. the way your "
"keyboard is manufactured. Modern keyboards that come with your computer "
"usually have two extra keys and are referred to as \"104-key\" models, which "
"is probably what you want if you do not know what kind of keyboard you "
"have.\n"
msgstr ""
"Овдје можете изабрати модел тастатуре. Ова поставка не зависи од распореда "
"тастатуре, већ се тиче „хардверског“ модела, тј. начина на који је тастатура "
"произведена. Модерне тастатуре обично имају два додатна тастера и познате су "
"као „104‑тастерски“ модели, што је добар основни избор ако не знате тачно "
"какву тастатуру имате.\n"

#. i18n: ectx: attribute (title), widget (QWidget, tabLayouts)
#: kcm_keyboard.ui:94
#, kde-format
msgid "Layouts"
msgstr "Распореди"

#. i18n: ectx: property (whatsThis), widget (QGroupBox, switchingPolicyGroupBox)
#: kcm_keyboard.ui:102
#, kde-format
msgid ""
"If you select \"Application\" or \"Window\" switching policy, changing the "
"keyboard layout will only affect the current application or window."
msgstr ""
"Ако изаберете смерницу мењања по програму или по прозору, промјена распореда "
"ће утицати само на текући програм или прозор."

#. i18n: ectx: property (title), widget (QGroupBox, switchingPolicyGroupBox)
#: kcm_keyboard.ui:105
#, kde-format
msgid "Switching Policy"
msgstr "Смјерница мијењања"

#. i18n: ectx: property (text), widget (QRadioButton, switchByGlobalRadioBtn)
#: kcm_keyboard.ui:111
#, kde-format
msgid "&Global"
msgstr "&Глобално"

#. i18n: ectx: property (text), widget (QRadioButton, switchByDesktopRadioBtn)
#: kcm_keyboard.ui:124
#, kde-format
msgid "&Desktop"
msgstr "По по&врши"

#. i18n: ectx: property (text), widget (QRadioButton, switchByApplicationRadioBtn)
#: kcm_keyboard.ui:134
#, kde-format
msgid "&Application"
msgstr "По &програму"

#. i18n: ectx: property (text), widget (QRadioButton, switchByWindowRadioBtn)
#: kcm_keyboard.ui:144
#, kde-format
msgid "&Window"
msgstr "По про&зору"

#. i18n: ectx: property (title), widget (QGroupBox, shortcutsGroupBox)
#: kcm_keyboard.ui:157
#, kde-format
msgid "Shortcuts for Switching Layout"
msgstr "Пречице за пребацивање распореда"

#. i18n: ectx: property (text), widget (QLabel, label)
#: kcm_keyboard.ui:163
#, kde-format
msgid "Main shortcuts:"
msgstr "Главне пречице:"

#. i18n: ectx: property (whatsThis), widget (QPushButton, xkbGrpShortcutBtn)
#: kcm_keyboard.ui:176
#, kde-format
msgid ""
"This is a shortcut for switching layouts which is handled by X.org. It "
"allows modifier-only shortcuts."
msgstr ""
"Ово је пречица за мијењање распореда коју обрађује Икс. Допушта чисто "
"модификаторске пречице."

#. i18n: ectx: property (text), widget (QPushButton, xkbGrpShortcutBtn)
#. i18n: ectx: property (text), widget (QPushButton, xkb3rdLevelShortcutBtn)
#: kcm_keyboard.ui:179 kcm_keyboard.ui:209
#, kde-format
msgctxt "no shortcut defined"
msgid "None"
msgstr "Никаква"

#. i18n: ectx: property (text), widget (QToolButton, xkbGrpClearBtn)
#. i18n: ectx: property (text), widget (QToolButton, xkb3rdLevelClearBtn)
#: kcm_keyboard.ui:186 kcm_keyboard.ui:216
#, kde-format
msgid "…"
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, label_3)
#: kcm_keyboard.ui:193
#, kde-format
msgid "3rd level shortcuts:"
msgstr "Пречице за трећи ниво:"

#. i18n: ectx: property (whatsThis), widget (QPushButton, xkb3rdLevelShortcutBtn)
#: kcm_keyboard.ui:206
#, kde-format
msgid ""
"This is a shortcut for switching to a third level of the active layout (if "
"it has one) which is handled by X.org. It allows modifier-only shortcuts."
msgstr ""
"Ово је пречица за прелазак на трећи ниво активног распореда (ако постоји) "
"коју обрађује Икс. Допушта чисто модификаторске пречице."

#. i18n: ectx: property (text), widget (QLabel, label_2)
#: kcm_keyboard.ui:223
#, kde-format
msgid "Alternative shortcut:"
msgstr "Алтернативна пречица:"

#. i18n: ectx: property (whatsThis), widget (KKeySequenceWidget, kdeKeySequence)
#: kcm_keyboard.ui:236
#, kde-format
msgid ""
"This is a shortcut for switching layouts. It does not support modifier-only "
"shortcuts and also may not work in some situations (e.g. if popup is active "
"or from screensaver)."
msgstr ""
"Ово је пречица за мењање распореда. Не подржава чисто модификаторске пречице "
"и може да не ради у неким случајевима (нпр. ако је активан искакач или на "
"чувару екрана)."

#. i18n: ectx: property (title), widget (QGroupBox, kcfg_configureLayouts)
#: kcm_keyboard.ui:261
#, kde-format
msgid "Configure layouts"
msgstr "Подеси распореде"

#. i18n: ectx: property (text), widget (QPushButton, addLayoutBtn)
#: kcm_keyboard.ui:275
#, kde-format
msgid "Add"
msgstr "Додај"

#. i18n: ectx: property (text), widget (QPushButton, removeLayoutBtn)
#: kcm_keyboard.ui:285
#, kde-format
msgid "Remove"
msgstr "Уклони"

#. i18n: ectx: property (text), widget (QPushButton, moveUpBtn)
#: kcm_keyboard.ui:295
#, kde-format
msgid "Move Up"
msgstr "Помери нагоре"

#. i18n: ectx: property (text), widget (QPushButton, moveDownBtn)
#: kcm_keyboard.ui:305
#, kde-format
msgid "Move Down"
msgstr "Помери надоле"

#. i18n: ectx: property (text), widget (QCheckBox, layoutLoopingCheckBox)
#: kcm_keyboard.ui:350
#, kde-format
msgid "Spare layouts"
msgstr "Резервни распореди"

#. i18n: ectx: property (text), widget (QLabel, label_5)
#: kcm_keyboard.ui:382
#, kde-format
msgid "Main layout count:"
msgstr "Број главних распореда:"

#. i18n: ectx: attribute (title), widget (QWidget, tabAdvanced)
#: kcm_keyboard.ui:412
#, kde-format
msgid "Advanced"
msgstr "Напредно"

#. i18n: ectx: property (text), widget (QCheckBox, kcfg_resetOldXkbOptions)
#: kcm_keyboard.ui:418
#, kde-format
msgid "&Configure keyboard options"
msgstr "&Подеси опције тастатуре"

#: kcm_keyboard_widget.cpp:204
#, kde-format
msgctxt "unknown keyboard model vendor"
msgid "Unknown"
msgstr "непознат"

#: kcm_keyboard_widget.cpp:206
#, kde-format
msgctxt "vendor | keyboard model"
msgid "%1 | %2"
msgstr "%1 | %2"

#: kcm_keyboard_widget.cpp:633
#, kde-format
msgctxt "no shortcuts defined"
msgid "None"
msgstr "ништа"

#: kcm_keyboard_widget.cpp:647
#, kde-format
msgid "%1 shortcut"
msgid_plural "%1 shortcuts"
msgstr[0] "%1 пречица"
msgstr[1] "%1 пречице"
msgstr[2] "%1 пречица"
msgstr[3] "%1 пречица"

# >> @title:column
#: kcm_view_models.cpp:200
#, kde-format
msgctxt "layout map name"
msgid "Map"
msgstr "мапа"

# >> @title:column
#: kcm_view_models.cpp:200
#, kde-format
msgid "Layout"
msgstr "распоред"

# >> @title:column
#: kcm_view_models.cpp:200
#, kde-format
msgid "Variant"
msgstr "варијанта"

# >> @title:column
#: kcm_view_models.cpp:200
#, kde-format
msgid "Label"
msgstr "етикета"

# >> @title:column
#: kcm_view_models.cpp:200
#, kde-format
msgid "Shortcut"
msgstr "пречица"

# >> @item:inlistbox layout variant
#: kcm_view_models.cpp:273
#, kde-format
msgctxt "variant"
msgid "Default"
msgstr "подразумевани"

#. i18n: ectx: property (text), widget (QLabel, label_2)
#: kcmmiscwidget.ui:31
#, kde-format
msgid "When a key is held:"
msgstr ""

#. i18n: ectx: property (text), widget (QRadioButton, accentMenuRadioButton)
#: kcmmiscwidget.ui:38
#, kde-format
msgid "&Show accented and similar characters "
msgstr ""

#. i18n: ectx: property (text), widget (QRadioButton, repeatRadioButton)
#: kcmmiscwidget.ui:45
#, kde-format
msgid "&Repeat the key"
msgstr ""

#. i18n: ectx: property (text), widget (QRadioButton, nothingRadioButton)
#: kcmmiscwidget.ui:52
#, kde-format
msgid "&Do nothing"
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, label)
#: kcmmiscwidget.ui:66
#, kde-format
msgid "Test area:"
msgstr "Пробна зона:"

#. i18n: ectx: property (toolTip), widget (QLineEdit, lineEdit)
#: kcmmiscwidget.ui:73
#, kde-format
msgid ""
"Allows to test keyboard repeat and click volume (just don't forget to apply "
"the changes)."
msgstr ""
"Омогућава да се испроба понављање тастатуре и гласност клика (не заборавите "
"да претходно примените измене)."

#. i18n: ectx: property (whatsThis), widget (QGroupBox, numlockGroupBox)
#: kcmmiscwidget.ui:82
#, kde-format
msgid ""
"If supported, this option allows you to setup the state of NumLock after "
"Plasma startup.<p>You can configure NumLock to be turned on or off, or "
"configure Plasma not to set NumLock state."
msgstr ""
"<p>Ако је подржана, овом опцијом одређујете стање тастера NumLock по "
"покретању Плазме.</p><p>Можете подесити да је NumLock укључен или искључен, "
"или да Плазма не мијења затечено стање.</p>"

# rewrite-msgid: /Plasma//
#. i18n: ectx: property (title), widget (QGroupBox, numlockGroupBox)
#: kcmmiscwidget.ui:85
#, kde-format
msgid "NumLock on Plasma Startup"
msgstr "NumLock на покретању"

#. i18n: ectx: property (text), widget (QRadioButton, radioButton1)
#: kcmmiscwidget.ui:97
#, kde-format
msgid "T&urn on"
msgstr "&Укључи"

#. i18n: ectx: property (text), widget (QRadioButton, radioButton2)
#: kcmmiscwidget.ui:104
#, kde-format
msgid "&Turn off"
msgstr "&Искључи"

#. i18n: ectx: property (text), widget (QRadioButton, radioButton3)
#: kcmmiscwidget.ui:111
#, kde-format
msgid "Leave unchan&ged"
msgstr "&Не дирај"

#. i18n: ectx: property (text), widget (QLabel, lblRate)
#: kcmmiscwidget.ui:148
#, kde-format
msgid "&Rate:"
msgstr "&Учестаност:"

#. i18n: ectx: property (whatsThis), widget (QSlider, delaySlider)
#. i18n: ectx: property (whatsThis), widget (QSpinBox, kcfg_repeatDelay)
#: kcmmiscwidget.ui:164 kcmmiscwidget.ui:202
#, kde-format
msgid ""
"If supported, this option allows you to set the delay after which a pressed "
"key will start generating keycodes. The 'Repeat rate' option controls the "
"frequency of these keycodes."
msgstr ""
"<html>Ако је подржана, овом опцијом задајете застој прије него што "
"притиснути тастер почне да шаље знакове. Брзину издавања знакова контролише "
"опција <i>Учестаност:</i>.</html>"

#. i18n: ectx: property (whatsThis), widget (QDoubleSpinBox, kcfg_repeatRate)
#. i18n: ectx: property (whatsThis), widget (QSlider, rateSlider)
#: kcmmiscwidget.ui:192 kcmmiscwidget.ui:212
#, kde-format
msgid ""
"If supported, this option allows you to set the rate at which keycodes are "
"generated while a key is pressed."
msgstr ""
"Ако је подржана, овом опцијом задајете учестаност којом се знакови шаљу док "
"је тастер притиснут."

# well-spelled: сек
#. i18n: ectx: property (suffix), widget (QDoubleSpinBox, kcfg_repeatRate)
#: kcmmiscwidget.ui:195
#, kde-format
msgid " repeats/s"
msgstr " знак./сек."

#. i18n: ectx: property (suffix), widget (QSpinBox, kcfg_repeatDelay)
#: kcmmiscwidget.ui:205
#, kde-format
msgid " ms"
msgstr " ms"

#. i18n: ectx: property (text), widget (QLabel, lblDelay)
#: kcmmiscwidget.ui:246
#, kde-format
msgid "&Delay:"
msgstr "&Застој:"

# >> @title:group
#: tastenbrett/main.cpp:52
#, fuzzy, kde-format
#| msgid "Keyboard Repeat"
msgctxt "app display name"
msgid "Keyboard Preview"
msgstr "Понављање тастера"

#: tastenbrett/main.cpp:54
#, fuzzy, kde-format
#| msgctxt "tooltip title"
#| msgid "Keyboard Layout"
msgctxt "app description"
msgid "Keyboard layout visualization"
msgstr "Распоред тастера"

#: tastenbrett/main.cpp:139
#, kde-format
msgctxt "@label"
msgid ""
"The keyboard geometry failed to load. This often indicates that the selected "
"model does not support a specific layout or layout variant. This problem "
"will likely also present when you try to use this combination of model, "
"layout and variant."
msgstr ""

#~ msgid "KDE Keyboard Control Module"
#~ msgstr "КДЕ контролни модул тастатуре"

#~ msgid "(c) 2010 Andriy Rysin"
#~ msgstr "© 2010, Андриј Рисин"

#~ msgid ""
#~ "<h1>Keyboard</h1> This control module can be used to configure keyboard "
#~ "parameters and layouts."
#~ msgstr ""
#~ "<h1>Тастатура</h1><p>Ова контролни модул служи за подешавање параметара и "
#~ "распореда тастатуре.</p>"

#~ msgid "KDE Keyboard Layout Switcher"
#~ msgstr "КДЕ‑ов мењач распореда тастатуре"

#~ msgctxt "short layout label - full layout name"
#~ msgid "%1 - %2"
#~ msgstr "%1 — %2"

# >> @item:inlistbox
#~ msgid "Any language"
#~ msgstr "било који језик"

# >> @label:listbox
#~ msgid "Layout:"
#~ msgstr "Распоред:"

# >> @label:listbox
#~ msgid "Variant:"
#~ msgstr "Варијанта:"

#~ msgid "Limit selection by language:"
#~ msgstr "Ограничи избор по језику:"

#~ msgid "Layout Indicator"
#~ msgstr "Показатељ распореда"

# >> @option:checkbox Layout Indicator
# rewrite-msgid: /layout//
#~ msgid "Show layout indicator"
#~ msgstr "Прикажи показатељ"

# >> @option:checkbox Layout Indicator
# rewrite-msgid: /layout//
#~ msgid "Show for single layout"
#~ msgstr "И при једном распореду"

# >> @option:checkbox Layout Indicator
#~ msgid "Show flag"
#~ msgstr "Са заставом"

# >> @option:checkbox Layout Indicator
#~ msgid "Show label"
#~ msgstr "Са етикетом"

# >> @option:checkbox Layout Indicator
#~ msgid "Show label on flag"
#~ msgstr "Са етикетом на застави"

#~ msgid "..."
#~ msgstr "..."

#~ msgid "Only up to %1 keyboard layout is supported"
#~ msgid_plural "Only up to %1 keyboard layouts are supported"
#~ msgstr[0] "Могуће је додати највише %1 распоред"
#~ msgstr[1] "Могуће је додати највише %1 распореда"
#~ msgstr[2] "Могуће је додати највише %1 распореда"
#~ msgstr[3] "Могуће је додати највише %1 распоред"

# >> @title:group
#~ msgid "Keyboard Repeat"
#~ msgstr "Понављање тастера"

#~ msgid "Turn o&ff"
#~ msgstr "&Искључи"

#~ msgid "&Leave unchanged"
#~ msgstr "&Не дирај"

#~ msgctxt "tooltip title"
#~ msgid "Keyboard Layout"
#~ msgstr "Распоред тастера"

#~ msgid "Configure Layouts..."
#~ msgstr "Подеси распореде..."
