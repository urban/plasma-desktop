# Translation of kcmaccess into Japanese.
# This file is distributed under the same license as the kdebase package.
# Noboru Sinohara <shinobo@leo.bekkoame.ne.jp>, 2002.
# Shinsaku Nakagawa <shinsaku@users.sourceforge.jp>, 2004.
# Shinichi Tsunoda <tsuno@ngy.1st.ne.jp>, 2005.
# UTUMI Hirosi <utuhiro78@yahoo.co.jp>, 2006
# Yukiko Bando <ybando@k6.dion.ne.jp>, 2006, 2007, 2008.
# Fumiaki Okushi <fumiaki.okushi@gmail.com>, 2015.
#
msgid ""
msgstr ""
"Project-Id-Version: kcmaccess\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-08-24 01:57+0000\n"
"PO-Revision-Date: 2015-05-22 22:11-0700\n"
"Last-Translator: Fumiaki Okushi <fumiaki.okushi@gmail.com>\n"
"Language-Team: Japanese <kde-jp@kde.org>\n"
"Language: ja\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#: kcmaccess.cpp:112
#, kde-format
msgid "AltGraph"
msgstr "AltGraph"

#: kcmaccess.cpp:114
#, kde-format
msgid "Hyper"
msgstr "Hyper"

#: kcmaccess.cpp:116
#, kde-format
msgid "Super"
msgstr "Super"

#: kcmaccess.cpp:126
#, kde-format
msgid "Press %1 while NumLock, CapsLock and ScrollLock are active"
msgstr "NumLock, CapsLock そして ScrollLock が有効なとき %1 を押す"

#: kcmaccess.cpp:127
#, kde-format
msgid "Press %1 while CapsLock and ScrollLock are active"
msgstr "CapsLock と ScrollLock が有効なとき %1 を押す"

#: kcmaccess.cpp:128
#, kde-format
msgid "Press %1 while NumLock and ScrollLock are active"
msgstr "NumLock と ScrollLock が有効なとき %1 を押す"

#: kcmaccess.cpp:129
#, kde-format
msgid "Press %1 while ScrollLock is active"
msgstr "ScrollLock が有効なとき %1 を押す"

#: kcmaccess.cpp:130
#, kde-format
msgid "Press %1 while NumLock and CapsLock are active"
msgstr "NumLock と CapsLock が有効なとき %1 を押す"

#: kcmaccess.cpp:131
#, kde-format
msgid "Press %1 while CapsLock is active"
msgstr "CapsLock が有効なとき %1 を押す"

#: kcmaccess.cpp:132
#, kde-format
msgid "Press %1 while NumLock is active"
msgstr "NumLock が有効なとき %1 を押す"

#: kcmaccess.cpp:133
#, kde-format
msgid "Press %1"
msgstr "%1 を押す"

#: kcmaccess.cpp:178
#, kde-format
msgid "Could not set gsettings for Orca: \"%1\" failed"
msgstr ""

#: kcmaccess.cpp:185
#, kde-format
msgid "Error: Could not launch \"orca --setup\""
msgstr ""

#. i18n: ectx: label, entry (SystemBell), group (Bell)
#: kcmaccessibilitybell.kcfg:9
#, fuzzy, kde-format
#| msgid "Use &system bell"
msgid "Use System Bell"
msgstr "システムベルを使う(&S)"

#. i18n: ectx: label, entry (CustomBell), group (Bell)
#: kcmaccessibilitybell.kcfg:13
#, fuzzy, kde-format
#| msgid "Use &system bell"
msgid "Customize system bell"
msgstr "システムベルを使う(&S)"

#. i18n: ectx: label, entry (CustomBellFile), group (Bell)
#: kcmaccessibilitybell.kcfg:17
#, kde-format
msgid "Sound file for the Bell"
msgstr ""

#. i18n: ectx: label, entry (VisibleBell), group (Bell)
#: kcmaccessibilitybell.kcfg:20
#, kde-format
msgid "Use a visual bell instead of a sound"
msgstr ""

#. i18n: ectx: label, entry (InvertScreen), group (Bell)
#: kcmaccessibilitybell.kcfg:24
#, kde-format
msgid "Invert the system colors on the bell"
msgstr ""

#. i18n: ectx: label, entry (VisibleBellColor), group (Bell)
#: kcmaccessibilitybell.kcfg:28
#, fuzzy, kde-format
#| msgid "Use &system bell"
msgid "Color of the Visible Bell"
msgstr "システムベルを使う(&S)"

#. i18n: ectx: label, entry (VisibleBellPause), group (Bell)
#: kcmaccessibilitybell.kcfg:32
#, kde-format
msgid "Duration of the Visible Bell"
msgstr ""

# ACCELERATOR changed by translator
#. i18n: ectx: label, entry (StickyKeys), group (Keyboard)
#: kcmaccessibilitykeyboard.kcfg:9
#, fuzzy, kde-format
#| msgid "Use &sticky keys"
msgid "Use sticky Keys"
msgstr "スティッキーキーを使う(&U)"

#. i18n: ectx: label, entry (StickyKeysLatch), group (Keyboard)
#: kcmaccessibilitykeyboard.kcfg:13
#, fuzzy, kde-format
#| msgid "&Lock sticky keys"
msgid "Lock the sticky keys"
msgstr "スティッキーキーをロックする(&L)"

# ACCELERATOR added by translator
#. i18n: ectx: label, entry (StickyKeysAutoOff), group (Keyboard)
#: kcmaccessibilitykeyboard.kcfg:17
#, kde-format
msgid "Turn sticky keys off when two keys are pressed simultaneously"
msgstr "二つのキーが同時に押されたらスティッキーキーを無効にする(&T)"

# ACCELERATOR changed by translator
#. i18n: ectx: label, entry (StickyKeysBeep), group (Keyboard)
#: kcmaccessibilitykeyboard.kcfg:21
#, fuzzy, kde-format
#| msgid "Use &sticky keys"
msgid "Beep on a sticky key press"
msgstr "スティッキーキーを使う(&U)"

#. i18n: ectx: label, entry (ToggleKeysBeep), group (Keyboard)
#: kcmaccessibilitykeyboard.kcfg:25
#, kde-format
msgid "Toggle keys beep"
msgstr ""

#. i18n: ectx: label, entry (KeyboardNotifyModifiers), group (Keyboard)
#: kcmaccessibilitykeyboard.kcfg:29
#, kde-format
msgid "Notify when a keyboard modifier is pressed"
msgstr ""

#. i18n: ectx: label, entry (AccessXTimeout), group (Keyboard)
#: kcmaccessibilitykeyboard.kcfg:33
#, kde-format
msgid "Use a timeout delay"
msgstr ""

#. i18n: ectx: label, entry (AccessXTimeoutDelay), group (Keyboard)
#: kcmaccessibilitykeyboard.kcfg:37
#, kde-format
msgid "Delay for timeout"
msgstr ""

#. i18n: ectx: label, entry (AccessXBeep), group (Keyboard)
#: kcmaccessibilitykeyboard.kcfg:41
#, kde-format
msgid "Use a beep for access"
msgstr ""

#. i18n: ectx: label, entry (SlowKeys), group (Keyboard)
#: kcmaccessibilitykeyboardfilters.kcfg:9
#, fuzzy, kde-format
#| msgid "&Use slow keys"
msgid "Use slow keypresses"
msgstr "スローキーを使う(&U)"

#. i18n: ectx: label, entry (SlowKeysDelay), group (Keyboard)
#: kcmaccessibilitykeyboardfilters.kcfg:13
#, kde-format
msgid "Delay for the key press"
msgstr ""

#. i18n: ectx: label, entry (SlowKeysPressBeep), group (Keyboard)
#: kcmaccessibilitykeyboardfilters.kcfg:17
#, kde-format
msgid "Beep on a slow keypress"
msgstr ""

#. i18n: ectx: label, entry (SlowKeysAcceptBeep), group (Keyboard)
#: kcmaccessibilitykeyboardfilters.kcfg:21
#, kde-format
msgid "Beep on an accepted keypress"
msgstr ""

#. i18n: ectx: label, entry (SlowKeysRejectBeep), group (Keyboard)
#: kcmaccessibilitykeyboardfilters.kcfg:25
#, kde-format
msgid "Beep on a rejected keypress"
msgstr ""

#. i18n: ectx: label, entry (BounceKeys), group (Keyboard)
#: kcmaccessibilitykeyboardfilters.kcfg:29
#, kde-format
msgid "Bounce Keys"
msgstr "バウンスキー (二度打ち防止機能)"

#. i18n: ectx: label, entry (BounceKeysDelay), group (Keyboard)
#: kcmaccessibilitykeyboardfilters.kcfg:33
#, kde-format
msgid "Use a delay for bouncing"
msgstr ""

#. i18n: ectx: label, entry (BounceKeysRejectBeep), group (Keyboard)
#: kcmaccessibilitykeyboardfilters.kcfg:37
#, kde-format
msgid "Beep if a bounce key event is rejected"
msgstr ""

#. i18n: ectx: label, entry (MouseKeys), group (Mouse)
#: kcmaccessibilitymouse.kcfg:9
#, kde-format
msgid "Use keys to control the mouse"
msgstr ""

#. i18n: ectx: label, entry (AccelerationDelay), group (Mouse)
#: kcmaccessibilitymouse.kcfg:13
#, kde-format
msgid "Delay for the mouse movement"
msgstr ""

#. i18n: ectx: label, entry (RepetitionInterval), group (Mouse)
#: kcmaccessibilitymouse.kcfg:17
#, kde-format
msgid "Repetition interval for the mouse movement"
msgstr ""

#. i18n: ectx: label, entry (AccelerationTime), group (Mouse)
#: kcmaccessibilitymouse.kcfg:21
#, kde-format
msgid "Time to hit maximum velocity"
msgstr ""

#. i18n: ectx: label, entry (MaxSpeed), group (Mouse)
#: kcmaccessibilitymouse.kcfg:25
#, kde-format
msgid "Maximum Velocity"
msgstr ""

#. i18n: ectx: label, entry (ProfileCurve), group (Mouse)
#: kcmaccessibilitymouse.kcfg:29
#, kde-format
msgid "Mouse Key Curve"
msgstr ""

#. i18n: ectx: label, entry (Gestures), group (Keyboard)
#: kcmaccessibilitymouse.kcfg:35
#, kde-format
msgid "Use Gestures"
msgstr ""

#. i18n: ectx: label, entry (GestureConfirmation), group (Keyboard)
#: kcmaccessibilitymouse.kcfg:39
#, kde-format
msgid "Ask for Confirmation for a gesture"
msgstr ""

#. i18n: ectx: label, entry (KeyboardNotifyAccess), group (Keyboard)
#: kcmaccessibilitymouse.kcfg:43
#, kde-format
msgid "Confirm access to the Keyboard"
msgstr ""

#. i18n: ectx: label, entry (Enabled), group (ScreenReader)
#: kcmaccessibilityscreenreader.kcfg:9 ui/ScreenReader.qml:17
#, kde-format
msgid "Enable Screen Reader"
msgstr ""

#: ui/Bell.qml:19
#, kde-format
msgid "Please choose an audio file"
msgstr ""

#: ui/Bell.qml:20
#, kde-format
msgctxt "Name filters in a file dialog"
msgid "Audio Files (*.ogg *.wav)"
msgstr ""

#: ui/Bell.qml:29
#, fuzzy, kde-format
#| msgid "Audible Bell"
msgid "Audible bell:"
msgstr "音声ベル"

#: ui/Bell.qml:30
#, kde-format
msgctxt "Enable the system bell"
msgid "Enable"
msgstr ""

#: ui/Bell.qml:41
#, kde-format
msgctxt "Defines if the system will use a sound system bell"
msgid "Custom sound:"
msgstr ""

#: ui/Bell.qml:74
#, kde-format
msgid "Search audio file for the system bell"
msgstr ""

#: ui/Bell.qml:75
#, kde-format
msgid "Button search audio file"
msgstr ""

#: ui/Bell.qml:91
#, fuzzy, kde-format
#| msgid "Visible Bell"
msgid "Visual bell:"
msgstr "視覚的ベル"

#: ui/Bell.qml:92
#, kde-format
msgctxt "Enable visual bell"
msgid "Enable"
msgstr ""

#: ui/Bell.qml:106
#, fuzzy, kde-format
#| msgid "I&nvert screen"
msgctxt "Invert screen on a system bell"
msgid "Invert Screen"
msgstr "スクリーンの色を反転させる(&N)"

#: ui/Bell.qml:123
#, kde-format
msgctxt "Flash screen on a system bell"
msgid "Flash"
msgstr ""

#: ui/Bell.qml:134
#, kde-format
msgctxt "Color of the system bell"
msgid "Color"
msgstr ""

#: ui/Bell.qml:148
#, fuzzy, kde-format
#| msgid "&Duration:"
msgctxt "Duration of the system bell"
msgid "Duration:"
msgstr "持続時間(&D):"

#: ui/KeyboardFilters.qml:18
#, fuzzy, kde-format
#| msgid "&Use slow keys"
msgid "Slow keys:"
msgstr "スローキーを使う(&U)"

#: ui/KeyboardFilters.qml:19
#, kde-format
msgctxt "Enable slow keys"
msgid "Enable"
msgstr ""

#: ui/KeyboardFilters.qml:22
#, kde-format
msgid ""
"For a key to be accepted, it has to be held until the set amount of time"
msgstr ""

#: ui/KeyboardFilters.qml:37
#, kde-format
msgctxt "Slow keys Delay"
msgid "Delay:"
msgstr ""

#: ui/KeyboardFilters.qml:54
#, fuzzy, kde-format
#| msgid "Use &system bell"
msgid "Ring system bell:"
msgstr "システムベルを使う(&S)"

# ACCELERATOR changed by translator
#: ui/KeyboardFilters.qml:55
#, fuzzy, kde-format
#| msgid "&Use system bell whenever a key is pressed"
msgctxt "Use system bell when a key is pressed"
msgid "when any key is &pressed"
msgstr "キーが押されたらシステムベルを使う(&T)"

# ACCELERATOR changed by translator
#: ui/KeyboardFilters.qml:69
#, fuzzy, kde-format
#| msgid "&Use system bell whenever a key is accepted"
msgctxt "Use system bell when a key is accepted"
msgid "when any key is &accepted"
msgstr "キー押下が受け付けられたらシステムベルを使う(&L)"

# ACCELERATOR changed by translator
#: ui/KeyboardFilters.qml:83
#, fuzzy, kde-format
#| msgid "&Use system bell whenever a key is rejected"
msgctxt "Use system bell when a key is rejected"
msgid "when any key is &rejected"
msgstr "キー押下が拒否されたらシステムベルを使う(&W)"

#: ui/KeyboardFilters.qml:100
#, fuzzy, kde-format
#| msgid "Bounce Keys"
msgid "Bounce keys:"
msgstr "バウンスキー (二度打ち防止機能)"

#: ui/KeyboardFilters.qml:101
#, kde-format
msgctxt "Bounce keys enable"
msgid "Enable"
msgstr ""

#: ui/KeyboardFilters.qml:104
#, kde-format
msgid "Ignore rapid, repeated keypresses of the same key"
msgstr ""

#: ui/KeyboardFilters.qml:119
#, kde-format
msgctxt "Bounce keys delay"
msgid "Delay:"
msgstr ""

# ACCELERATOR changed by translator
#: ui/KeyboardFilters.qml:134
#, fuzzy, kde-format
#| msgid "&Use system bell whenever a key is rejected"
msgid "Ring system bell when rejected"
msgstr "キー押下が拒否されたらシステムベルを使う(&W)"

#: ui/main.qml:23
#, fuzzy, kde-format
#| msgid "&Bell"
msgctxt "System Bell"
msgid "Bell"
msgstr "ベル(&B)"

#: ui/main.qml:28
#, fuzzy, kde-format
#| msgid "&Modifier Keys"
msgctxt "System Modifier Keys"
msgid "Modifier Keys"
msgstr "修飾キー(&M)"

#: ui/main.qml:33
#, fuzzy, kde-format
#| msgid "&Keyboard Filters"
msgctxt "System keyboard filters"
msgid "Keyboard Filters"
msgstr "キーボードフィルタ(&K)"

#: ui/main.qml:38
#, kde-format
msgctxt "System mouse navigation"
msgid "Mouse Navigation"
msgstr ""

#: ui/main.qml:43
#, kde-format
msgctxt "System mouse navigation"
msgid "Screen Reader"
msgstr ""

# ACCELERATOR removed by translator
#: ui/ModifierKeys.qml:16
#, fuzzy, kde-format
#| msgid "Sticky Keys"
msgid "Sticky keys:"
msgstr "スティッキーキー (順次入力機能)"

#: ui/ModifierKeys.qml:17
#, kde-format
msgctxt "Enable sticky keys"
msgid "Enable"
msgstr ""

#: ui/ModifierKeys.qml:28
#, kde-format
msgctxt "Lock sticky keys"
msgid "Lock"
msgstr ""

#: ui/ModifierKeys.qml:42
#, kde-format
msgid "Disable when two keys are held down"
msgstr ""

# ACCELERATOR changed by translator
#: ui/ModifierKeys.qml:54
#, fuzzy, kde-format
#| msgid "&Use system bell whenever a key is pressed"
msgid "Ring system bell when modifier keys are used"
msgstr "キーが押されたらシステムベルを使う(&T)"

#: ui/ModifierKeys.qml:71
#, kde-format
msgid "Feedback:"
msgstr ""

# ACCELERATOR changed by translator
#: ui/ModifierKeys.qml:72
#, fuzzy, kde-format
#| msgid "&Use system bell whenever a key is accepted"
msgid "Ring system bell when locking keys are toggled"
msgstr "キー押下が受け付けられたらシステムベルを使う(&L)"

# ACCELERATOR added by translator
#: ui/ModifierKeys.qml:83
#, fuzzy, kde-format
#| msgid ""
#| "Use Plasma's system notification mechanism whenever a modifier or locking "
#| "key changes its state"
msgid "Show notification when modifier or locking keys are used"
msgstr "修飾キーやロックキーの状態が変わったら Plasma のシステム通知を使う(&W)"

#: ui/ModifierKeys.qml:95
#, fuzzy, kde-format
#| msgid "Configure &Notifications..."
msgid "Configure Notifications…"
msgstr "通知を設定(&N)..."

#: ui/MouseNavigation.qml:17
#, kde-format
msgid "Use number pad to move cursor:"
msgstr ""

#: ui/MouseNavigation.qml:18
#, kde-format
msgid "Enable"
msgstr ""

#: ui/MouseNavigation.qml:29
#, kde-format
msgid "When a gesture is used:"
msgstr ""

#: ui/MouseNavigation.qml:30
#, kde-format
msgid "Display a confirmation dialog"
msgstr ""

#: ui/MouseNavigation.qml:41
#, fuzzy, kde-format
#| msgid "Use &system bell"
msgid "Ring the System Bell"
msgstr "システムベルを使う(&S)"

#: ui/MouseNavigation.qml:52
#, fuzzy, kde-format
#| msgid "Notification"
msgid "Show a notification"
msgstr "通知"

#: ui/MouseNavigation.qml:68
#, kde-format
msgid "Acceleration delay:"
msgstr ""

#: ui/MouseNavigation.qml:82
#, kde-format
msgid "Repeat interval:"
msgstr ""

#: ui/MouseNavigation.qml:96
#, kde-format
msgid "Acceleration time:"
msgstr ""

#: ui/MouseNavigation.qml:110
#, kde-format
msgid "Maximum speed:"
msgstr ""

#: ui/MouseNavigation.qml:124
#, kde-format
msgid "Pointer acceleration:"
msgstr ""

#: ui/ScreenReader.qml:28
#, kde-format
msgid "Launch Orca Screen Reader Configuration…"
msgstr ""

#: ui/ScreenReader.qml:41
#, kde-format
msgid ""
"Please note that you may have to log out or reboot once to allow the screen "
"reader to work properly."
msgstr ""

#: ui/ScreenReader.qml:42
#, kde-format
msgid ""
"It appears that the Orca Screen Reader is not installed. Please install it "
"before trying to use this feature, and then log out or reboot"
msgstr ""
